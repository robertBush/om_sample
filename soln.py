from collections import deque
from itertools import islice
from typing import List, OrderedDict, Tuple
from uuid import uuid4, UUID

gate_use_duration_seconds = 1


class OrderedDictWithFirst(OrderedDict):
    def first(self):
        """Return the first element from an ordered dictionary.
        Raise StopIteration if the collection is empty.
        """
        return self[next(iter(self))]


class Person:
    def __init__(self, arrival_time: int, direction: int, gate_time: int = None):
        self.arrival_time = arrival_time
        self.id = uuid4()
        self.direction = direction
        self.gate_time = gate_time

    def __repr__(self):
        return f"{str(self.id)[0:4]} arriving at {self.arrival_time}; {'exiting' if int(self.direction) else 'entering'} at {self.gate_time}"


def sort_people_into_queues(people: List[Person]) -> Tuple[deque[Person], deque[Person]]:
    """sort people into entering and exiting deques"""
    entering = deque()
    exiting = deque()
    for person in people:
        if int(person.direction):
            exiting.append(person)
            continue
        entering.append(person)
    return entering, exiting


def get_consecutive_people_from_line(line: deque[Person]) -> List[Person]:
    """gets a list of consecutive line members (same-line members that arrived less than 2 seconds apart)"""
    result = [line.popleft()]
    try:
        while line[0].arrival_time - result[-1].arrival_time < 2:
            result.append(line.popleft())
    except IndexError:
        pass
    return result


def pop_all_as_list(line: deque[Person]) -> List[Person]:
    result = list(line.copy())
    line.clear()
    return result


def get_next_uses(entering: deque[Person], exiting: deque[Person]) -> List[Person]:
    """gets a list of next people to use the gate from the ever-decreasing deques of 'entering' and 'exiting' people"""
    if len(entering) < 1:
        return pop_all_as_list(line=exiting)
    if len(exiting) < 1:
        return pop_all_as_list(line=entering)
    if entering[0].arrival_time < exiting[0].arrival_time:
        return get_consecutive_people_from_line(entering)
    return get_consecutive_people_from_line(exiting)


def create_people(
        times: List[int],
        directions: List[int],
) -> List[Person]:
    """create People objects from arrival times and directions. In addition to more readable code,
    the unique ids assigned to people will be used to retrieve their original index-order for the final result"""
    result = []
    for index, time in enumerate(times):
        result.append(Person(arrival_time=time, direction=directions[index]))
    return result


def get_ordered_entries(people: List[Person]) -> OrderedDictWithFirst[UUID, Person]:
    """gets all people in the order they will go through the gate. Keyed by ID for easier retrieval as a hash"""
    entering, exiting = sort_people_into_queues(people=people)
    result = OrderedDictWithFirst()
    while len(entering) > 0 or len(exiting) > 0:
        for person in get_next_uses(
                entering=entering,
                exiting=exiting,
        ):
            result[person.id] = person
    return result


def solution(times: List[int], directions: List[int]) -> List[int]:
    people = create_people(times=times, directions=directions)
    people_ordered_by_arrival = get_ordered_entries(people=people)

    # calculate each person's gate-use-time based on their ordered arrivals
    first_person = people_ordered_by_arrival.first()
    first_person.gate_time = first_person.arrival_time
    current_gate_time = first_person.gate_time
    # the islice function serves here to iterate and skip first element
    for person in islice(people_ordered_by_arrival.values(), 1, None):
        if person.arrival_time > current_gate_time:
            current_gate_time = person.arrival_time
        else:
            current_gate_time = current_gate_time + gate_use_duration_seconds
        person.gate_time = current_gate_time
    # use the original list of people (in the original order) to create a result with gate-use times ordered by the original indexes
    return [people_ordered_by_arrival[person.id].gate_time for person in people]


if __name__ == "__main__":
    assert solution(times=[0, 0, 1, 5], directions=[0, 1, 1, 0]) == [2, 0, 1, 5]
    assert solution(times=[0, 1, 1, 3, 3], directions=[0, 1, 0, 0, 1]) == [
        0,
        2,
        1,
        4,
        3,
    ]
